#!/bin/bash

echo -e "\e[42mGo Error Factory\e[0m"
echo "Make sure you have a DSN set as an environment variable!"
echo -e "Run docker-compose run --rm app bash, then run any of the following scripts:\n"
echo "go run failed-request/failed-request.go <good or bad url>"
echo "go run file-not-found/file-not-found.go"
echo "go run unique/unique.go"
