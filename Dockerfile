FROM golang:1.12-alpine

RUN apk update && apk upgrade && apk add --no-cache bash git openssh

# Set the Current Working Directory inside the container
WORKDIR /app

RUN go get github.com/getsentry/sentry-go

# # Copy the source from the current directory to the Working Directory inside the container
COPY . .
