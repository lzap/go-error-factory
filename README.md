# How to run

Ensure Docker and Docker Compose are installed

`docker-compose up`

# How to make errors

Set the Sentry DSN to your Sentry or Glitchtip project's DSN

- `cp docker-compose.yml docker-compose.override.yml`
- Edit the override file to set the `SENTRY_DSN`
- docker-compose up will show available commands to trigger an error.

# Gopher

Riffed on an image created by Takuya Ueda (https://twitter.com/tenntenn, https://github.com/golang-samples/gopher-vector). Licensed under the Creative Commons 3.0 Attributions license.
