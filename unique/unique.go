package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
)

func main() {
	err := sentry.Init(sentry.ClientOptions{
		Dsn:   os.Getenv("SENTRY_DSN"),
		Debug: true,
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}

	defer sentry.Flush(2 * time.Second)

	var error = errors.New("Unique error! " + fmt.Sprintf("%v", time.Now().UnixNano()))
	sentry.CaptureException(error)
	log.Printf("reported to Sentry: %s", error)
}
